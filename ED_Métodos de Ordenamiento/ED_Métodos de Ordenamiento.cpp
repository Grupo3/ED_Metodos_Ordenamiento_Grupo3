// ED_M�todos de Ordenamiento.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <stdlib.h>

using namespace std;

struct nodo
{
	int valor;
	struct nodo *sgte;
	struct nodo *ant;
};

typedef struct nodo *Tlista;

//Funcion para ingresar los datos
void Insertar(Tlista &lista, int valor);

//La 8 funciones de ornamiento
void bubbleSort(Tlista &lista);
void selectionSort(Tlista &lista);
void insertionSort(Tlista &lista);
void swappingSort(Tlista &lista);
void shellSort(Tlista &lista);
void quickSort(Tlista &lista, Tlista temp);
void mergeSort(Tlista &lista);
void radixSort(Tlista &lista);

//Funciones necesarias para el mergeSort
void partir(Tlista &cab, Tlista &prim, Tlista &fin);
Tlista listando(Tlista &a, Tlista &b);

//Funciones necesarias para el radixSort
int numeroCifras(int valor);
int indices(int valor, int pos);
Tlista eliminarInicio(Tlista &lista);
void insertarAux(Tlista &lista, Tlista nuevo);

//Funciones para mostrar la lista
void MostrarIda(Tlista &lista);
void MostrarVuelta(Tlista &lista);

int main()
{
	Tlista lista = NULL;
	Tlista listaAux = NULL;
	int valor, op;
	char rpta;

	do {
		system("cls");
		cout << endl;
		cout << "\t\t\t����������������������������������ͻ" << endl;
		cout << "\t\t\t�     Metodos de Ordenamiento      �" << endl;
		cout << "\t\t\t����������������������������������͹" << endl;
		cout << "\t\t\t� 1.-  Insertar Elementos          �" << endl;
		cout << "\t\t\t� 2.-  Ordenamiento de Burbuja     �" << endl;
		cout << "\t\t\t� 3.-  Ordenamiento de Seleccion   �" << endl;
		cout << "\t\t\t� 4.-  Ordenamiento de Insercion   �" << endl;
		cout << "\t\t\t� 5.-  Ordenamiento de Intercambio �" << endl;
		cout << "\t\t\t� 6.-  Ordenamiento Shell          �" << endl;
		cout << "\t\t\t� 7.-  Ordenamiento Quick          �" << endl;
		cout << "\t\t\t� 8.-  Ordenamiento Merge          �" << endl;
		cout << "\t\t\t� 9.-  Ordenamiento Radix          �" << endl;
		cout << "\t\t\t� 10.- Salir                       �" << endl;
		cout << "\t\t\t����������������������������������ͼ" << endl;

		cout << "\t\t\tEscoja una Opcion: ";

		do {
			cin >> op;
			cout << endl;
			if (op<0 || op>10) {
				cout << "Escoja denuevo: ";
			}
		} while (op<0 || op>10);

		switch (op)
		{
			case 1: {
				do {

					system("cls");
					cout << "\nIngrese un Valor: "; cin >> valor;
					cout << endl;
					Insertar(lista, valor);
					MostrarIda(lista);
					MostrarVuelta(lista);
					cout << "Desea continuar <s/n>?: "; cin >> rpta;
					cout << endl;

				} while (rpta == 's' || rpta == 'S');
				break;
			}

			case 2: {
				bubbleSort(lista);
				system("cls");
				MostrarIda(lista);
				MostrarVuelta(lista);
				system("pause");
				break;
			}

			case 3: {
				selectionSort(lista);
				system("cls");
				MostrarIda(lista);
				MostrarVuelta(lista);
				system("pause");
				break;
			}

			case 4:
			{
				do {

					system("cls");
					insertionSort(listaAux);
					MostrarIda(listaAux);
					MostrarVuelta(listaAux);
					cout << "Desea continuar <s/n>?: "; cin >> rpta;
					cout << endl;

				} while (rpta == 's' || rpta == 'S');
				break;
			}

			case 5:
			{
				swappingSort(lista);
				system("cls");
				MostrarIda(lista);
				MostrarVuelta(lista);
				system("pause");
				break;
			}

			case 6:
			{
				do {
					system("cls");
					shellSort(lista);
					MostrarIda(lista);
					MostrarVuelta(lista);
					cout << "\nLa lista esta ordenada <s/n>?: "; cin >> rpta;
					cout << endl;

				} while (rpta == 'n' || rpta == 'N');
				system("pause");
				break;
			}

			case 7:
			{
				system("cls");
				quickSort(lista, NULL);
				MostrarIda(lista);
				MostrarVuelta(lista);
				system("pause");
				break;
			}

			case 8:
			{
				system("cls");
				mergeSort(lista);
				MostrarIda(lista);
				MostrarVuelta(lista);
				system("pause");
				break;
			}

			case 9:
			{
				system("cls");
				radixSort(lista);
				MostrarIda(lista);
				MostrarVuelta(lista);
				system("pause");
				break;
			}

			case 10:
			{
				system("cls");
				cout << "\nEL PROGRAMA SE CERRARA, GRACIAS" << endl << endl;
				return 0;
			}

		}

	} while (op > 0 && op < 10);

}

//Funcion para ingresar los valor por el final de la lista
void Insertar(Tlista &lista, int valor)
{
	Tlista p = lista;
	Tlista nuevo = new (struct nodo);
	nuevo->valor = valor;
	nuevo->sgte = NULL;
	if (lista == NULL) {
		lista = nuevo;
	}
	else {
		while (p->sgte != NULL) {
			p = p->sgte;
		}
		p->sgte = nuevo;
	}
}

//Funcion para el ordenamiento por Burbuja
void bubbleSort(Tlista &lista)
{
	Tlista p = lista;
	int valorAux;
	bool flag = false;
	if (lista == NULL) {
		cout << "\nLa Lista Esta Vacia" << endl;
	}
	else {
		if (lista->sgte == NULL) {
			cout << "\nLa Lista Esta Ordenada" << endl;
		}
		else {
			while (p->sgte != NULL) {
				if (p->valor > p->sgte->valor) {
					valorAux = p->valor;
					p->valor = p->sgte->valor;
					p->sgte->valor = valorAux;
					flag = true;
				}
				p = p->sgte;
				if (p->sgte == NULL) {
					if (flag == true) {
						p = lista;
						flag = false;
					}
					else {
						return;
					}
				}
			}
		}
	}
}

//Funcion para el odenamiento por Seleccion
void selectionSort(Tlista &lista)
{
	Tlista p = lista;
	Tlista q = p;
	Tlista aux = NULL;
	int valorAux;
	if (lista == NULL) {
		cout << "\nLa Lista Esta Vacia" << endl;
	}
	else {
		if (lista->sgte == NULL) {
			cout << "\nLa Lista Esta Ordenada" << endl;
		}
		else {
			while (p->sgte != NULL) {
				valorAux = p->valor;
				q = p->sgte;
				do {
					if (q->valor < valorAux) {
						valorAux = q->valor;
						aux = q;
					}
					if (q->sgte != NULL) {
						q = q->sgte;
					}
				} while (q->sgte != NULL);
				aux->valor = p->valor;
				p->valor = valorAux;
				p = p->sgte;
			}
		}
	}
}

//Funcion para el ordenamiento por Insercion
void insertionSort(Tlista &lista)
{
	int numero;

	cout << "\nIngrese un Valor: "; cin >> numero;
	cout << endl;

	Tlista aux = NULL;
	Tlista nuevo = new (struct nodo);
	nuevo->valor = numero;
	nuevo->sgte = NULL;
	Tlista p = lista;
	Tlista final = lista;
	if (lista == NULL) {
		lista = nuevo;
	}
	else {
		if (lista->sgte == NULL) {
			if (nuevo->valor <= lista->valor) {
				nuevo->sgte = lista;
				lista = nuevo;
			}
			else {
				lista->sgte = nuevo;
			}
		}
		else {
			while (final->sgte != NULL) {
				final = final->sgte;
			}

			if (numero <= lista->valor) {
				nuevo->sgte = lista;
				lista = nuevo;
			}
			else if (numero >= final->valor) {
				final->sgte = nuevo;
			}
			else {
				while (p->valor <= numero) {
					aux = p;
					p = p->sgte;
				}
				nuevo->sgte = p;
				aux->sgte = nuevo;
			}
		}
	}
}

//Funcion para el ordenamiento por Intercambio
void swappingSort(Tlista &lista)
{
	Tlista primero = lista;
	Tlista p = NULL;
	Tlista aux = NULL;
	int valorAux;
	int cont = 1;
	int i = 1;
	int j;
	if (lista == NULL) {
		cout << "\nLa Lista Esta Vacia" << endl;
	}
	else {
		if (lista->sgte == NULL) {
			cout << "\nLa Lista Esta Ordenada" << endl;
		}
		else {
			while (primero->sgte != NULL) {
				primero = primero->sgte;
				cont++;
			}
			primero = lista;
			do {
				p = primero;
				j = i + 1;
				while (j <= cont) {
					p = p->sgte;
					if (primero->valor > p->valor) {
						valorAux = p->valor;
						p->valor = primero->valor;
						primero->valor = valorAux;
					}
					j++;
				}
				primero = primero->sgte;
				i++;
			} while (i <= cont);
		}
	}
}

//Funcion para el ordenamiento Shell
void shellSort(Tlista &lista)
{
	Tlista p = lista;
	Tlista q = NULL;
	int valorAux;
	int cont = 1;
	int i = 0;

	if (lista == NULL) {
		cout << "\nLa lista esta vacia" << endl;

	}
	else {
		if (lista->sgte == NULL) {
			cout << "\nLa lista esta ordenada" << endl;

		}
		else {
			while (p->sgte != NULL) {
				p = p->sgte;
				cont++;
			}
			p = lista;

			do {
				cont = cont / 2;
				while (i < cont) {
					p = p->sgte;
					i++;
					q = p;
				}

				p = lista;
				while (q != NULL) {
					if (p->valor > q->valor) {
						valorAux = p->valor;
						p->valor = q->valor;
						q->valor = valorAux;
					}
					q = q->sgte;
					p = p->sgte;
				}

				i = 0;
				p = lista;

			} while (cont > 0);
		}
	}
}

//Funcion para el ordenamiento Quick
void quickSort(Tlista &lista, Tlista temp)
{
	if (lista != temp) {

		Tlista der;
		Tlista izq = lista;
		Tlista q = lista;
		while (q->sgte != NULL) {
			q = q->sgte;
		}
		der = q;

		bool flag = true;
		Tlista p = izq;

		int pivote = izq->valor;

		if (der == izq)
		{

			flag = false;
		}
		else {
			flag = true;
		}

		do {

			while (flag && izq != der) {

				while (p != temp) {
					der = p;
					p = p->sgte;
				}

				if (pivote>der->valor) {

					izq->valor = der->valor;
					der->valor = pivote;

					flag = false;

				}
				temp = der;
				p = izq;
			}
			while (izq != der) {
				izq = izq->sgte;
				if (pivote<izq->valor)
				{
					der->valor = izq->valor;
					izq->valor = pivote;
					flag = true;
					break;
				}
			}
		} while (izq != der);

		quickSort(lista, izq);
		quickSort(der->sgte, NULL);

	}
}

//Funciones para el ordenamiento Merge
void partir(Tlista &cab, Tlista &prim, Tlista &fin) {

	Tlista aux1 = NULL;
	Tlista aux2 = NULL;

	if (cab == NULL || cab->sgte == NULL) {
		prim = cab;
		fin = NULL;
	}
	else {
		aux2 = cab;
		aux1 = cab->sgte;

		while (aux1 != NULL) {
			aux1 = aux1->sgte;

			if (aux1 != NULL) {
				aux2 = aux2->sgte;
				aux1 = aux1->sgte;
			}
		}

		prim = cab;
		fin = aux2->sgte;
		aux2->sgte = NULL;
	}
}

Tlista listando(Tlista &a, Tlista &b) {

	Tlista p = NULL;

	if (a == NULL) {
		return b;
	}
	else if (b == NULL) {
		return a;
	}

	if (a->valor <= b->valor) {
		p = a;
		p->sgte = listando(a->sgte, b);
	}
	else {
		p = b;
		p->sgte = listando(a, b->sgte);
	}

	return p;
}

void mergeSort(Tlista &lista) {

	Tlista p = lista;
	Tlista a = NULL;
	Tlista b = NULL;

	if (p == NULL || p->sgte == NULL) {

		return;
	}

	partir(p, a, b);

	mergeSort(a);
	mergeSort(b);

	lista = listando(a, b);
}

//Funciones para el ordenamiento Radix

int numeroCifras(int valor)
{
	int valorAux = valor;
	int cont = 0;

	while (valorAux != 0) {
		valorAux = valorAux / 10;
		cont++;
	}

	return cont;
}

int indices(int valor, int pos)
{
	int valorAux = valor;
	int indice;
	int cont = 0;
	while (valorAux != 0 && cont != pos) {
		indice = valorAux % 10;
		valorAux = valorAux / 10;
		cont++;
	}

	if (cont != pos) {
		return 0;
	}

	return indice;
}

Tlista eliminarInicio(Tlista &lista)
{
	if (lista == NULL) {
		return NULL;
	}
	else {
		Tlista p = lista;
		lista = lista->sgte;
		p->sgte = NULL;
		return p;
	}
}

void insertarAux(Tlista &lista, Tlista nuevo)
{
	Tlista p = lista;
	if (lista == NULL) {
		lista = nuevo;
	}
	else {
		while (p->sgte != NULL) {
			p = p->sgte;
		}
		p->sgte = nuevo;
	}
}

void radixSort(Tlista &lista)
{
	Tlista p = lista;
	int maxCifras;
	int indice;
	int cont = 0;
	int mayor = 0;

	if (lista == NULL) {
		cout << "\nLa lista esta vacia" << endl;
	}
	else {
		while (p != NULL) {
			cont++;
			if (p->valor > mayor) {
				mayor = p->valor;
			}
			p = p->sgte;
		}

		maxCifras = numeroCifras(mayor);

		for (int i = 0; i < maxCifras; i++) {
			Tlista lista_0 = NULL;
			Tlista lista_1 = NULL;
			Tlista lista_2 = NULL;
			Tlista lista_3 = NULL;
			Tlista lista_4 = NULL;
			Tlista lista_5 = NULL;
			Tlista lista_6 = NULL;
			Tlista lista_7 = NULL;
			Tlista lista_8 = NULL;
			Tlista lista_9 = NULL;

			while (lista != NULL) {
				p = eliminarInicio(lista);
				indice = indices(p->valor, i + 1);

				switch (indice)
				{
				case 0:
				{
					insertarAux(lista_0, p);
					break;
				}

				case 1:
				{
					insertarAux(lista_1, p);
					break;
				}

				case 2:
				{
					insertarAux(lista_2, p);
					break;
				}

				case 3:
				{
					insertarAux(lista_3, p);
					break;
				}

				case 4:
				{
					insertarAux(lista_4, p);
					break;
				}

				case 5:
				{
					insertarAux(lista_5, p);
					break;
				}

				case 6:
				{
					insertarAux(lista_6, p);
					break;
				}

				case 7:
				{
					insertarAux(lista_7, p);
					break;
				}

				case 8:
				{
					insertarAux(lista_8, p);
					break;
				}

				case 9:
				{
					insertarAux(lista_9, p);
					break;
				}

				}
			}

			insertarAux(lista, lista_0);
			insertarAux(lista, lista_1);
			insertarAux(lista, lista_2);
			insertarAux(lista, lista_3);
			insertarAux(lista, lista_4);
			insertarAux(lista, lista_5);
			insertarAux(lista, lista_6);
			insertarAux(lista, lista_7);
			insertarAux(lista, lista_8);
			insertarAux(lista, lista_9);

		}
	}
}

//Funciones para mostrar los datos de la lista doblemente enlazada tanto de IDA como de VUELTA
void MostrarIda(Tlista &lista)
{
	Tlista p = lista;
	if (lista == NULL) {
		cout << "\nLA LISTA ESTA VACIA" << endl;
	}
	else {
		cout << "\nIda: ";
		while (p->sgte != NULL) {
			cout << p->valor << " -> ";
			p = p->sgte;
		}
		cout << p->valor << " -> NULL" << endl;
	}
}

void MostrarVuelta(Tlista &lista)
{
	Tlista p = lista;
	if (lista == NULL) {
		cout << "\nLA LISTA ESTA VACIA" << endl;
	}
	else {
		cout << "Vuelta: NULL <- ";
		while (p->sgte != NULL) {
			cout << p->valor << " <- ";
			p = p->sgte;
		}
		cout << p->valor << endl << endl;
	}
}
